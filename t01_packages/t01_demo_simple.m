%% clear and restore path...
clear all global

restoredefaultpath

%% add the toolbox to the path...
addpath('my_toolbox');

%% now we can call the functions...
first_package.my_fun();

second_package.my_fun();